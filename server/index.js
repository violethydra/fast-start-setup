const colors = require('colors');

const port = 5000;

const config = {
	url: {
		register: '/register',
		auth: '/auth',
		users: '/users'
	},
	
	api: {
		guest: '/guest',
		user: '/user',
		partner: '/partner',
		client: '/client',
		admin: '/admin'
	},

	db: {
		name: 'dashboard'
	},

	user: {
		collection: 'database',
		login: 'dummyUser',
		pass: 'dummyUser'
	},

	error: '[ERROR]:'.red,
	env: '[ENV]:'.yellow,
	path: '[PATH]:'.magenta,
	server: '[SERVER]:'.red,

	ENV: process.env.NODE_ENV || 'development',
	PORT: process.env.PORT || port,
	URL: process.env.BASE_URL || `http://localhost:${port}`,
	JWT_SECRET: process.env.JWT_SECRET || 'mysecret'
};

module.exports = config;
