const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const session = require('express-session');
const FileStore = require('connect-loki')(session);
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');

const __log = require('fancy-log');
const __init = require('./__init/');

const app = express();

// Passport Config
require('./config/passport')(passport);

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
	.connect(db, { useNewUrlParser: true })
	.then(() => console.log('MongoDB Connected'))
	.catch(err => console.log(err));

// EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Express body parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Express session
app.use(
	session({
		secret: 'secret',
		store: new FileStore(),
		coockie: { 
			path: '/',
			maxAge: 60 * 60 * 1000
		},
		resave: false,
		saveUninitialized: false
	})
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Global variables
app.use((req, res, next) => {
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	res.locals.error = req.flash('error');
	next();
});

// Routes
app.use('/', require('./routes'));
app.use('/main', require('./routes/main'));
app.use('/users', require('./routes/users'));
app.use('/api', require('./routes/api'));
app.use('/api', require('./routes/api/roles'));

// add static
const showPath = 'public';
app.use('/main', express.static(showPath));
// app.use('/users', express.static(showPath));
// app.use('/api', express.static(showPath));

// Watch
app.listen(__init.PORT, () => {
	__log(__init.path, __filename);
	__log(__init.env, __init.ENV);
	__log(`${__init.server} Server started on port ${__init.PORT}`);
});