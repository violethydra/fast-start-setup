const root = {
	client: 'app',
	server: 'server',
	pub: 'public',
	dev: 'development'
};


const __init = {
	isDevelopment: !process.env.NODE_ENV	|| process.env.NODE_ENV === 'development',
	isPublic: process.env.NODE_ENV === 'public',
	
	inPub: `${root.client}/${root.pub}`,
	inPubCss: `${root.client}/${root.pub}/css`,
	
	inDev: `${root.client}/${root.dev}`,
	inDevApps: `${root.client}/${root.dev}/components`,
	
	devTemp: `${root.client}\\${root.dev}\\tmp`,
	devPlug: `${root.client}\\${root.dev}\\components\\plugins`
};

module.exports = __init;