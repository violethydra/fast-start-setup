/* ==== IMPORT PARAMS ==== */

/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const inDev = 'development';
const inDevApps = `${inDev}/components`;
const inPub = 'public';
/* ==== ----- ==== */


const noDevState = !process.env.NODE_ENV || process.env.NODE_ENV === inDev;


/* ==== Replace URL or Links ==== */
const __cfg = {
	stylus: {
		sprite: {
			svg: {
				mode: {
					css: {
						dest: '.',
						bust: !noDevState,
						sprite: 'sprite.svg',
						layouts: 'vertical',
						prefix: '$',
						dimensions: true,
						render: { styl: { dest: 'sprite.styl' } }
					}
				}			
			}
		}
	}
};
/* ==== ----- ==== */


module.exports = (nameTask, _run, combiner, src, dest, errorConfig, __init) =>
() => combiner(
		src(`${inDevApps}/img/__sprite/**/*.svg`, { allowEmpty: true }),
		_run.svgSprite(__cfg.stylus.sprite.svg),
		_run.if('*.styl',
			dest(`${inDev}/tmp`),
			dest(`${inPub}/media/img`))
		
		).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));