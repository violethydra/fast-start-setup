/* ==== IMPORT PARAMS ==== */

module.exports = (nameTask, _run, combiner, src, dest, errorConfig, __init) =>
	() => combiner(

		src(`${__init.inPub}/*.html`),
		_run.if(__init.isPublic, _run.htmlmin({ collapseWhitespace: true })),
		_run.rename({ suffix: '.min' }),
		dest(__init.inPub)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
