/* ==== IMPORT PARAMS ==== */

module.exports = (nameTask, _run, combiner, src, dest, errorConfig, __init) =>
	() => combiner(
		src(`${__init.inPub}/js/connect.js`, { allowEmpty: true }),
		_run.if(__init.isPublic, _run.babelMinify()),
		_run.rename({ suffix: '.min' }),
		dest(__init.inPubJs)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
