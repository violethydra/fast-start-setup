/* ==== IMPORT PARAMS ==== */
import path from 'path';
import log from 'fancy-log';

import { lastRun } from 'gulp';
import fs from 'fs';
// import ttf2woff from 'fancy-log';
// import ttf2woff2 from 'fancy-log';

/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const inDev = 'development';
const inDevApps = `${inDev}/components`;
const inPub = 'public';

let myPATH = [];
let myPATHisEnd = [];

// ttf,eot,otf,woff,woff2
// console.log(path.extname(`${inDevApps}/fonts/**/*`).substr(1));
/* ==== ----- ==== */

const mySuperFunc = () => {
	fs.readdir(`./${inDevApps}/fonts/test`, (err, files) => {
		 myPATH += files.map(f => `${inDevApps}/fonts/TEST/${f}/ttf/*.ttf`);
		 myPATHisEnd += files.map(f => `${inDevApps}/fonts/TEST/END/${f}`);
		 return console.log('myPATH', [myPATH]);
	});
};
 
mySuperFunc();
const sinceReplace = x => (`${x}`.replace(/-/gi, ':'));

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(
		src([`${inDevApps}/fonts/TEST/Ubuntu/ttf/*.ttf`,`${inDevApps}/fonts/TEST/GoogleSans/ttf/*.ttf`], { allowEmpty: true, since: lastRun(sinceReplace(nameTask)) }),
		_run.ttf2woff(),
		dest(`${inDevApps}/fonts/TEST/END/`)
		// dest(`${[myPATHisEnd]}/woff`)
	)
	// && combiner(
	// 	src([myPATH], { allowEmpty: true, since: lastRun(sinceReplace(nameTask)) }),
	// 	_run.ttf2woff2(),
	// 	dest(`${inDevApps}/fonts/TEST/woff2`)
	// )

	.on('error',
		_run.notify.onError(err => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
