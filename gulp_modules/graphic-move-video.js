/* ==== IMPORT PARAMS ==== */

/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const inDev = 'development';
const inDevApps = `${inDev}/components`;
const inPub = 'public';
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, errorConfig, __init) =>
	() => combiner(

		src(`${inDevApps}/video/**/*.{avi,mp4,mkv}`),
		_run.size({ title: '[Video size] :' }),
		// _run.if(isPublic, _run.size()),
		dest(`${inPub}/media/video`)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
