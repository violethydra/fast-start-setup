// ============================
//    Name: index.js
// ============================

import $moveScrollUP from './modules/moveScrollUP';
import $openMyBurger from './modules/openMyBurger';

const startPackage = () => {
	console.log('[DOM]:', 'DOMContentLoaded', true);

	new $moveScrollUP({
		selector: '.js__moveScrollUP',
		speed: 8
	}).run();

	new $openMyBurger({
		burger: '.js__navHamburger',
		navbar: '.js__navHamburgerOpener'
	}).run();
};

const start = async () => {
	try {
		await startPackage();
	} catch (err) {
		console.error('System: ', 'I think shit happens 😥 ');
		console.error(err);
	}
};

const addCss = (fileName) => {
	const link = document.createElement('link');
	link.type = 'text/css';
	link.rel = 'stylesheet';
	link.href = fileName;

	document.head.appendChild(link);
};

if (typeof window !== 'undefined' && window && window.addEventListener) {

	if (navigator.userAgent.includes('Firefox')) addCss('css/firefox.css');
	if (navigator.userAgent.includes('Edge')) addCss('css/edge.css');

	document.addEventListener('DOMContentLoaded', start(), false);
}
