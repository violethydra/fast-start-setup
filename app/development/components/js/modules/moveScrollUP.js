export default class $moveScrollUP {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		if (!this.selector) return;

		this.count = Number(Math.abs(init.speed)) || 8;
		this.speed = (this.count <= 20) ? this.count : 8;
		this.myPos = window.pageYOffset;
		this.getScroll = 0;
		this.speedScroll = this.speed;
	}

	static info() { console.log('[MODULE]:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.eventHandlers();
		}
	}

	eventHandlers() {
		this.selector.addEventListener('click', event => this.clickedArrow(event), false);
		window.addEventListener('scroll', event => this.scrolledDown(event), false);
	}

	clickedArrow(event) {
		this.getScroll = document.documentElement.scrollTop;
		if (this.getScroll >= 1) {
			window.requestAnimationFrame(this.clickedArrow.bind(this));
			window.scrollTo(0, this.getScroll - this.getScroll / this.speedScroll);
		}
	}

	scrolledDown(event) {
		this.myPos = window.pageYOffset;
		if (this.myPos >= 100) this.selector.classList.add('show');
		else this.selector.classList.remove('show');
	}
}
