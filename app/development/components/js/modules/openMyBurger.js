export default class $openMyBurger {
	constructor(init) {
		this.selector = document.querySelector(init.burger);
		if (!this.selector) return;
		
		this.navbar = document.querySelector(init.navbar);
		this.errorText = '💀 Ouch, database error...';
	}

	static info() { console.log('[MODULE]:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.eventHandlers();
		}
	}
	
	eventHandlers() {
		this.selector.addEventListener('click', event => this.openBurger(event), false);
	}
	
	openBurger(event) {
		const elem = this.navbar.querySelector('DIV');
		const count = elem.childElementCount;
		const countString = `${count * 2 + count / 2}rem`;

		elem.classList.toggle('open');
		elem.style.paddingBottom = '';

		if (elem.classList.contains('open')) elem.style.paddingBottom = countString;

		this.selector.classList.toggle('open');
	}
}
